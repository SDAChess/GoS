if myHero.charName ~= 'Garen' then return end

class "Garen"
require'DamageLib'
require'MapPositionGos'

function Garen:LoadSpells()
  Q = {range = 300, delay = 0, speed = math.Huge}
  W = {range = 300, delay = 0, speed = math.Huge}
  E = {range = 325, delay = 0, speed = math.Huge, width = 325}
  R = {range = 400, delay = 0, speed = math.Huge}
end

function Garen:Tick()
if myHero.dead or Game.IsChatOpen() == true or IsRecalling() == true or ExtLibEvade and ExtLibEvade.Evading == true then return end
    if  _G.SDK.Orbwalker.Modes[_G.SDK.ORBWALKER_MODE_HARASS] then
        self:Harass()
    end
    if  _G.SDK.Orbwalker.Modes[_G.SDK.ORBWALKER_MODE_COMBO] then
        self:Combo()
    end
    if not  _G.SDK.Orbwalker.Modes[_G.SDK.ORBWALKER_MODE_COMBO] then
        self:AutoUlt()
    end
    self:AutoIgnite()
    self:RDamage()
end

---------------------Calculation Part---------------------------------------------------------
function Garen:RDamage(target)
  local target = _G.SDK.TargetSelector:GetTarget(1000, _G.SDK.DAMAGE_TYPE_PHYSICAL)
  if target then
  local Rlevel = myHero:GetSpellData(_R).level
  local BaseRdamage = ({175,350,525})[Rlevel]
  local missingHealth = (target.maxHealth - target.health)
  local multiplier = ({0.143,0.166,0.2})[Rlevel]
  local dmg = BaseRdamage + (multiplier * missingHealth)
  local passive = GetBuffData(target,"garenpassiveenemytarget")
  if passive.count >= 1 then Rdmg = dmg
  elseif passive.count == 0 then Rdmg = CalcMagicDamage(target, dmg) end
  return Rdmg
end
end
function OnLoad() Garen() end
function Distance(pos1, pos2)
	local _pos1 = Vector(pos1)
	local _pos2 = Vector(pos2)
	return _pos1:DistanceTo(_pos2)
end

function IsImmune(unit) ----Determines if there is any immune buff such as Trynda's R so it won't ultimate.
    if type(unit) ~= "userdata" then error("{IsImmune}: bad argument #1 (userdata expected, got " .. type(unit) .. ")") end
    for i, buff in pairs(GetBuffs(unit)) do
        if (buff.name == "KindredRNoDeathBuff" or buff.name == "UndyingRage") and GetPercentHP(unit) <= 10 then
            return true
        end
        if buff.name == "VladimirSanguinePool" or buff.name == "JudicatorIntervention" then
            return true
        end
    end
    return false
end

function IsRecalling()  ---Determines if the champion is recalling
    for K, Buff in pairs(GetBuffs(myHero)) do
        if Buff.name == "recall" and Buff.duration > 0 then
            return true
        end
    end
    return false
end

function IsReady(islot)
	local _data = myHero:GetSpellData(islot)
	return _data.currentCd == 0 and _data.level > 0 and _data.mana <= myHero.mana and Game.CanUseSpell(islot) == READY
end

------------------------------Menu and Drawing part----------------------------

function Garen:LoadMenu()
  self.Menu = MenuElement({type = MENU, id = "Garen", name = "Garen", leftIcon = HeroIcon})
  self.Menu:MenuElement({type = MENU, id ="Combo", name = "Combo Settings"})
  self.Menu.Combo:MenuElement({id = "Q", name ="Use Q", value = true})
  self.Menu.Combo:MenuElement({id = "W", name ="Use W", value = false})
  self.Menu.Combo:MenuElement({id = "E", name ="Use E", value = true})
  self.Menu.Combo:MenuElement({id = "R", name ="Use R", value = true})
  self.Menu:MenuElement({type = MENU, id = "Harass", name = "Harass Settings"})
  self.Menu.Harass:MenuElement({id = "Q", name ="Use Q", value = true})
  self.Menu:MenuElement({type = MENU, id = "Misc", name = "Misc. Settings"})
  self.Menu.Misc:MenuElement({id = "AutoUlt", name = "Auto Ult if killable", value = true})
  self.Menu.Misc:MenuElement({id = "AutoIgnite", name = "Auto Ignite if killable", value = true})

  self.Menu:MenuElement({type = MENU, id = "Drawing", name = "Drawing settings"})
  self.Menu.Drawing:MenuElement({id = "DrawQ", name = "Draw Q range", value = true})
  self.Menu.Drawing:MenuElement({id = "DrawE", name = "Draw E range", value = true})
  self.Menu.Drawing:MenuElement({id = "DrawR", name = "Draw R range", value = true})
  self.Menu.Drawing:MenuElement({id = "DrawKillable", name = "Draw if enemy killable with R", value = true})
end

function Garen:Draw()
    if myHero.dead then return end
    if self.Menu.Drawing.DrawQ:Value() then Draw.Circle(myHero.pos, myHero:GetSpellData(_Q).range, 1, Draw.Color(255, 0, 191, 255)) end
    if self.Menu.Drawing.DrawR:Value() then Draw.Circle(myHero.pos, myHero:GetSpellData(_R).range, 1, Draw.Color(255, 0, 0, 255)) end
    if self.Menu.Drawing.DrawE:Value() then Draw.Circle(myHero.pos, myHero:GetSpellData(_E).range, 1, Draw.Color(255, 100, 200, 0)) end
    if self.Menu.Drawing.DrawKillable:Value() then
        if  _target.health < Rdmg then
                        Draw.Text("Killable with R", 25,  _target.pos2D.x - 38,  _target.pos2D.y + 10, Draw.Color(255,255,255,255))
        end
    end
end
-----------------------------Mode part
function Garen:Combo()
 local _target = _G.SDK.TargetSelector:GetTarget(1000, _G.SDK.DAMAGE_TYPE_PHYSICAL)
  if _target or Rdmg then
    if self.Menu.Combo.W:Value() and IsReady(_W) then
        Control.CastSpell(HK_W)
    end

    if self.Menu.Combo.E:Value() and IsReady(_E) and  Distance(myHero.pos, _target.pos) <= 310 then
            Control.CastSpell(HK_E)
    end

    if self.Menu.Combo.Q:Value() and IsReady(_Q) then Distance(myHero.pos, _target.pos) <= 300
            Control.CastSpell(HK_Q)
    end

    if self.Menu.Combo.R:Value() and IsReady(_R) and _target.health < Rdmg and Distance(myHero.pos, _target.pos) <= 400 then
        Control.CastSpell(HK_R, _target.pos)
    end
  end
end

function Garen:Harass() -----Auto Q when in range of Auto Attack
  if self.Menu.Harass.Q:Value() and IsReady(_Q) and Distance(myHero.pos, _target.pos) <= 300 then
      Control.CastSpell(HK_Q)
  end
end

--------------------------- Auto Ult/Ignite
function Garen:AutoUlt() ----------------Auto Ult function
  	local _target = _G.SDK.TargetSelector:GetTarget(1000, _G.SDK.DAMAGE_TYPE_MAGICAL)
 if Rmdg or _target then
    if _target.health < Rdmg and IsReady(_R) and Distance(myHero.pos, _target.pos) <= 400 then
    if not IsImmune(_target) then
            Control.CastSpell(HK_R, _target.pos)
        end
    end
  end
end

function Garen:AutoIgnite() --------------------Auto Ignite function
  local _target = _G.SDK.TargetSelector:GetTarget(1000, _G.SDK.DAMAGE_TYPE_MAGICAL)
  local IgniteDamage = (55 + 25 * myHero.levelData.lvl)
  if _target.health < IgniteDmg then
                  if not IsImmune(enemy) then
                      if myHero:GetSpellData(SUMMONER_1).name == "SummonerDot" and IsReady(SUMMONER_1) then
                          Control.CastSpell(HK_SUMMONER_1, _target.pos)
                      elseif myHero:GetSpellData(SUMMONER_2).name == "SummonerDot" and IsReady(SUMMONER_2) then
                          Control.CastSpell(HK_SUMMONER_2, _target.pos)
                      end
            end
      end
end

--------------------------------------------Init

function Garen:__init()
  print("GarenSama by SDA loaded")
  self:LoadSpells()
	self:LoadMenu()
  self:AutoUlt()
  self:AutoIgnite()
	Callback.Add("Tick", function() self:Tick() end)
	Callback.Add("Draw", function() self:Draw() end)
end
