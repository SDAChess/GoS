if myHero.charName ~= "Cassiopeia" then PrintChat("Cassiopeia detected") return end


class "Carriopeia"
require("DamageLib")
require'MapPositionGos'

function OnLoad() Carriopeia() end

function Carriopeia:__init()
    Q = {range = myHero:GetSpellData(_Q).range, delay = myHero:GetSpellData(_Q).delay, speed = myHero:GetSpellData(_Q).speed, width = myHero:GetSpellData(_Q).width}
    W = {range = myHero:GetSpellData(_W).range, delay = myHero:GetSpellData(_W).delay, speed = myHero:GetSpellData(_W).speed, width = myHero:GetSpellData(_W).width}
    E = {range = myHero:GetSpellData(_E).range, delay = myHero:GetSpellData(_E).delay, speed = myHero:GetSpellData(_E).speed, width = myHero:GetSpellData(_E).width}
    R = {range = myHero:GetSpellData(_R).range, delay = myHero:GetSpellData(_R).delay, speed = myHero:GetSpellData(_R).speed, width = myHero:GetSpellData(_R).width}
    self:Menu()
    Callback.Add("Tick", function() self:Tick() end)
    Callback.Add("Draw", function() self:Draw() end)
end

function Carriopeia:Menu()
    self.Menu = MenuElement({type = MENU, name = "Carriopeia", id = "Carriopeia"})

    self.Menu:MenuElement({type = MENU, id ="Combo", name = "Combo Settings"})
    self.Menu.Combo:MenuElement({id = "Q", name ="Use Q", value = true})
    self.Menu.Combo:MenuElement({id = "W", name ="Use W", value = false})
    self.Menu.Combo:MenuElement({id = "E", name ="Use E", value = true})
    self.Menu.Combo:MenuElement({id = "R", name ="Use R", value = true})

    self.Menu:MenuElement({type = MENU, id = "Harass", name = "Harass Settings"})
    self.Menu.Harass:MenuElement({id = "Q", name ="Use Q", value = true})
    self.Menu.Harass:MenuElement({id = "W", name ="Use W", value = false})
    self.Menu.Harass:MenuElement({id = "E", name ="Use E", value = true})

    self.Menu:MenuElement({type = MENU, id = "Misc", name = "Misc. Settings"})
    self.Menu.Misc:MenuElement({id = "AutoQ", name = "Auto Q enemies in range")
    self.Menu.Misc:MenuElement({id = "AutoE", name = "Auto E enemies in range poisened")
    self.Menu.Misc:MenuElement({id = "Interrupt", name = "Auto Interrupt Spells")
    self.Menu.Misc:MenuElement({id = "AssistedUlt", name = "Assisted Ultimate", key = string.byte("T")})
    self.Menu.Misc:MenuElement({id = "AutoUlt", name = "Automatic ultimate if X enemies", value = 3, min = 1, max = 5, step = 1})

    self.Menu:MenuElement({type = Menu, id = "Drawing", name = "Drawing settings"})
    self.Menu.Drawing:MenuElement({id = "DrawQ", name = "Draw Q range", value = true})
    self.Menu.Drawing:MenuElement({id = "DrawW", name = "Draw W range", value = true})
    self.Menu.Drawing:MenuElement({id = "DrawE", name = "Draw E range", value = true})
    self.Menu.Drawing:MenuElement({id = "DrawR", name = "Draw R range", value = true})
end

function Carriopeia:Draw()
  if myHero.dead then return end
    if self.Menu.Drawing.DrawQ:Value() then Draw.Circle(myHero.pos, myHero:GetSpellData(_Q).range, 1, Draw.Color(255, 0, 191, 255)) end
    if self.Menu.Drawing.DrawW:Value() then Draw.Circle(myHero.pos, myHero:GetSpellData(_W).range, 1, Draw.Color(255, 65, 105, 225)) end
    if self.Menu.Drawing.DrawE:Value() then Draw.Circle(myHero.pos, myHero:GetSpellData(_E).range, 1, Draw.Color(255, 30, 144, 255)) end
    if self.Menu.Drawing.DrawR:Value() then Draw.Circle(myHero.pos, myHero:GetSpellData(_R).range, 1, Draw.Color(255, 0, 0, 255)) end
end


function Carriopeia:Tick()
  	Carriopeia:GetEnemyAngles()
    if myHero.dead or  IsRecalling()  or IsEvading() or IsDelaying() then return end
    if NextSpellCast > Game.Timer() then return end
    --Disable autoattacks when have mana to E
    if Menu.Skills.Combo:Value() and CurrentPctMana(myHero) > 15 then
      SetAttack(false)
    else
      SetAttack(true)
    end

	  --We check for any combo key pressed
        local target = _G.SDK.TargetSelector:GetTarget(2000)

        if target and _G.SDK.Orbwalker.Modes[_G.SDK.ORBWALKER_MODE_FLEE] then
            self:Flee()
        end

        if  _G.SDK.Orbwalker.Modes[_G.SDK.ORBWALKER_MODE_COMBO] then
            self:Combo(target)
        end

        if target  and _G.SDK.Orbwalker.Modes[_G.SDK.ORBWALKER_MODE_HARASS] then
            self:Harass(target)
        end
    if not _G.SDK.Orbwalker.Modes[_G.SDK.ORBWALKER_MODE_FLEE] or _G.SDK.Orbwalker.Modes[_G.SDK.ORBWALKER_MODE_COMBO] or G.SDK.Orbwalker.Modes[_G.SDK.ORBWALKER_MODE_HARASS]  then
        if Ready(_R) then
      		Cassiopeia:AutoR()
      	end

      	if Ready(_E) then
      		Cassiopeia:AutoE()
      	end

      	if Ready(_Q) and CurrentPctMana(myHero) >= Menu.Skills.Q.Mana:Value() then
      		Cassiopeia:AutoQ()
      	end
    end
end

local _enemyAngles = {} --Allows us to see where the enemy is looking.
function Carriopeia:GetEnemyAngles()
	for i = 1, LocalGameHeroCount() do
		local t = LocalGameHero(i)
		if t and t.alive and t.visible and t.isEnemy then
			if t.pathing.hasMovePath then
				if _enemyAngles[t.networkID] == nil then _enemyAngles[t.networkID] = {dir = (t.posTo - t.pos):Normalized(), updateTick = Game.Timer()}
				else _enemyAngles[t.networkID].dir = (t.posTo - t.pos):Normalized() _enemyAngles[t.networkID].updateTick = Game.Timer() end
			elseif t.activeSpell and t.activeSpell.valid and t.activeSpell.placementPos then
				d = (Vector(t.activeSpell.placementPos.x, t.activeSpell.placementPos.y, t.activeSpell.placementPos.z) - t.pos):Normalized()
				if _enemyAngles[t.networkID] == nil then _enemyAngles[t.networkID] = {dir = d, updateTick = Game.Timer()}
				else _enemyAngles[t.networkID].dir = d _enemyAngles[t.networkID].updateTick = Game.Timer() end
			end
		end
	end
end


function Carriopeia:IsPoisened(target, duration)
	if not duration then
		duration = 0
	end
	for i = 1, target.buffCount do
		local buff = target:GetBuff(i)
		if buff.duration > duration and buff.type == 23 then
			return true
		end
	end
end
